<?php
namespace Drupal\custom\Plugin\Block;
use Drupal\Core\Block\BlockBase;
/**
 * Provides a 'custom' Block.
 *
 * @Block(
 *   id = "job_home",
 *   admin_label = @Translation("Custom: Việc làm tốt nhất")
 * )
 */
class JobHomeBlock extends BlockBase
{

	/**
	 * {@inheritdoc}
	 **/
	public function build() {
		$views_hot = views_embed_view('jobs', 'block_1');
		$views_new = views_embed_view('jobs', 'block_2');
		return array(
			'#markup' => '
			<div>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#hot" aria-controls="hot" role="tab" data-toggle="tab">Việc làm tốt nhất</a></li>
					<li role="presentation"><a href="#new" aria-controls="new" role="tab" data-toggle="tab">Việc làm mới nhất</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="hot">'. render($views_hot) .'</div>
					<div role="tabpanel" class="tab-pane" id="new">'. render($views_new) .'</div>
				</div>

			</div>',
		);
	}
}