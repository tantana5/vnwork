<?php

/* themes/bootstrap/templates/block/block--system.html.twig */
class __TwigTemplate_903ddec4e17118c606e06ec67c348ac814c6e471056b3c3a12c3222794d2cd04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 9
        $this->parent = $this->loadTemplate("block--bare.html.twig", "themes/bootstrap/templates/block/block--system.html.twig", 9);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "block--bare.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_05ffb2328db0ceded23f0cb0714f48eca1d4f9ac380ed31e60b6b8297d3e4925 = $this->env->getExtension("native_profiler");
        $__internal_05ffb2328db0ceded23f0cb0714f48eca1d4f9ac380ed31e60b6b8297d3e4925->enter($__internal_05ffb2328db0ceded23f0cb0714f48eca1d4f9ac380ed31e60b6b8297d3e4925_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/bootstrap/templates/block/block--system.html.twig"));

        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_05ffb2328db0ceded23f0cb0714f48eca1d4f9ac380ed31e60b6b8297d3e4925->leave($__internal_05ffb2328db0ceded23f0cb0714f48eca1d4f9ac380ed31e60b6b8297d3e4925_prof);

    }

    public function getTemplateName()
    {
        return "themes/bootstrap/templates/block/block--system.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 9,);
    }

    public function getSource()
    {
        return "{#
/**
 * @file
 * Theme override for system blocks.
 *
 * @ingroup templates
 */
#}
{% extends \"block--bare.html.twig\" %}
";
    }
}
