<?php

/* themes/vnwork/templates/pages/page.html.twig */
class __TwigTemplate_b2d86675f44980451bc94f916fd0b62801a4cdfc8dd9d50553a90747dab8da6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e913094a1b32cde42d7ac0fb03e350d5df8f9e5df5b63faf65e713fc74d7fae5 = $this->env->getExtension("native_profiler");
        $__internal_e913094a1b32cde42d7ac0fb03e350d5df8f9e5df5b63faf65e713fc74d7fae5->enter($__internal_e913094a1b32cde42d7ac0fb03e350d5df8f9e5df5b63faf65e713fc74d7fae5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/vnwork/templates/pages/page.html.twig"));

        $tags = array("if" => 45);
        $filters = array("t" => 46);
        $functions = array("drupal_block" => 64);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array('t'),
                array('drupal_block')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 40
        echo "
<div class=\"wrapper\">
  <header id=\"header\">
    <div class=\"container\">
      <div class=\"row\">
        ";
        // line 45
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            // line 46
            echo "          <div class=\"col-xs-3 col-sm-2 logo\"><a href=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["front_page"]) ? $context["front_page"] : null), "html", null, true));
            echo "\"><img src=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["logo"]) ? $context["logo"] : null), "html", null, true));
            echo "\" alt=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Home")));
            echo "\" /></a></div>
        ";
        }
        // line 48
        echo "        
        ";
        // line 49
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array())) {
            // line 50
            echo "          <div class=\"col-xs-9 col-sm-10 header-content\">
            <div class=\"row\">
              ";
            // line 52
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
            echo "
            </div>
          </div>
        ";
        }
        // line 56
        echo "      </div>
    </div> <!-- End container -->
  </header> <!-- End header -->
    
  <div class=\"wrapper-content\">
    <div class=\"content-top\">
      <div class=\"container\">
        <h1 class=\"page-title\">";
        // line 63
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "#title", array(), "array"), "html", null, true));
        echo "</h1>
        ";
        // line 64
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->env->getExtension('custom')->drupalBlock("fmall_breadcrumbs"), "html", null, true));
        echo "
      </div> <!-- End container -->
    </div> <!-- End content top -->
    
    ";
        // line 68
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array())) {
            // line 69
            echo "      <div class=\"main-content\">
        <div class=\"container\">
          ";
            // line 71
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
            echo "
        </div>
      </div>
    ";
        }
        // line 75
        echo "  </div> <!-- End main content -->
  
  ";
        // line 77
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_top", array())) {
            // line 78
            echo "    <div class=\"footer-top\">
      <div class=\"container\">
        <div class=\"description\">";
            // line 80
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_top", array()), "html", null, true));
            echo "</div>
      </div>
    </div>
  ";
        }
        // line 84
        echo "  
  <footer id=\"footer\">
    <div class=\"container\">
      ";
        // line 87
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array())) {
            // line 88
            echo "        <div class=\"row\">
          ";
            // line 89
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 92
        echo "    </div> <!-- End container -->
  </footer> <!-- End footer -->
</div> <!-- End wrapper -->";
        
        $__internal_e913094a1b32cde42d7ac0fb03e350d5df8f9e5df5b63faf65e713fc74d7fae5->leave($__internal_e913094a1b32cde42d7ac0fb03e350d5df8f9e5df5b63faf65e713fc74d7fae5_prof);

    }

    public function getTemplateName()
    {
        return "themes/vnwork/templates/pages/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 92,  141 => 89,  138 => 88,  136 => 87,  131 => 84,  124 => 80,  120 => 78,  118 => 77,  114 => 75,  107 => 71,  103 => 69,  101 => 68,  94 => 64,  90 => 63,  81 => 56,  74 => 52,  70 => 50,  68 => 49,  65 => 48,  55 => 46,  53 => 45,  46 => 40,);
    }

    public function getSource()
    {
        return "{#
/**
 * @file
 * Bartik's theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template normally located in the
 * core/modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.content: The main content of the current page.
 * - page.footer: Items for the fifth footer column.
 * - page.breadcrumb: Items for the breadcrumb region.
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}

<div class=\"wrapper\">
  <header id=\"header\">
    <div class=\"container\">
      <div class=\"row\">
        {% if logo %}
          <div class=\"col-xs-3 col-sm-2 logo\"><a href=\"{{ front_page }}\"><img src=\"{{ logo }}\" alt=\"{{ 'Home'|t }}\" /></a></div>
        {% endif %}
        
        {% if page.header %}
          <div class=\"col-xs-9 col-sm-10 header-content\">
            <div class=\"row\">
              {{ page.header }}
            </div>
          </div>
        {% endif %}
      </div>
    </div> <!-- End container -->
  </header> <!-- End header -->
    
  <div class=\"wrapper-content\">
    <div class=\"content-top\">
      <div class=\"container\">
        <h1 class=\"page-title\">{{page['#title']}}</h1>
        {{ drupal_block('fmall_breadcrumbs') }}
      </div> <!-- End container -->
    </div> <!-- End content top -->
    
    {% if page.content %}
      <div class=\"main-content\">
        <div class=\"container\">
          {{ page.content }}
        </div>
      </div>
    {% endif %}
  </div> <!-- End main content -->
  
  {% if page.footer_top %}
    <div class=\"footer-top\">
      <div class=\"container\">
        <div class=\"description\">{{ page.footer_top }}</div>
      </div>
    </div>
  {% endif %}
  
  <footer id=\"footer\">
    <div class=\"container\">
      {% if page.footer %}
        <div class=\"row\">
          {{ page.footer }}
        </div>
      {% endif %}
    </div> <!-- End container -->
  </footer> <!-- End footer -->
</div> <!-- End wrapper -->";
    }
}
